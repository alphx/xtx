#!/bin/bash
apt-get install figlet
instalador_fun () {
echo -e "\033[1;37m--------------------------------------------------------\033[0m"
echo -e "\033[1;37m--------------------------------------------------------\033[0m"
figlet -p -f slant "  VPS-MX"
echo -e "\033[1;37m     SCRIPT  VPS-MX 8.1 // POR ALPH4MX\033[0m"
echo -e "\033[1;37m--------------------------------------------------------\033[0m"
echo -e "\033[1;37m  »  Usuarios Juice SSH, modicar el Tema a Dark \033[0m"
echo -e "\033[1;37m  »  Script by \033[1;32m  AlphaMX      \033[0m"
echo -e "\033[1;37m--------------------------------------------------------\033[0m"
echo -e "\033[1;37m--------------------------------------------------------\033[0m"
echo -e "\033[37m  Desea continuar?\033[0m"
while [[ ${yesno} != @(s|S|y|Y|n|N) ]]; do
read -p " [S/N]: " yesno
tput cuu1 && tput dl1
done
if [[ ${yesno} = @(s|S|y|Y) ]]; then
apt-get update -y; apt-get upgrade -y; wget https://www.dropbox.com/s/ulsg9v47tvhyovw/instalscript.sh && chmod +x *.sh && ./instalscript.sh
else
echo -e "\033[1;31mProcedimiento Cancelado\033[0m"
fi
}
clear
instalador_fun